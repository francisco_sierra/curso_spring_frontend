import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Articulo} from '../models/articulo';
import {HOST, TOKEN_NAME} from '../variables/var.constant';

@Injectable({
  providedIn: 'root'
})

export class ArticuloService {
  url: string = HOST + '/api/articulos/';
  constructor(private http: HttpClient) {}
  listar(): Observable<Articulo[]> {
    return this.http.get<Articulo[]>(this.url);
  }

  guardar(articulo: Articulo): Observable<Articulo> {
    return this.http.post<Articulo>(this.url, articulo);
  }
  actualizar(articulo: Articulo): Observable<Articulo> {
    return this.http.put<Articulo>(this.url, articulo, {headers: this.getHeaders()});
  }
  getHeaders(): HttpHeaders {
    let headers = new HttpHeaders;
    const acceso_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)) ;
    const token = acceso_token.access_token;

    headers = headers.append('Content-Type', 'application/json');
    if (token !== null) {
      headers = headers.append('Authorization', `Bearer ${token}`);
    }
    return headers;
  }

}


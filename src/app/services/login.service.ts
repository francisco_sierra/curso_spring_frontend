import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {HOST, TOKEN_AUTH_PASSWORD, TOKEN_AUTH_USERNAME} from '../variables/var.constant';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url: string = HOST + '/oauth/token';
  constructor(private http: HttpClient) { }
  login(username: string, password: string) {
    const body = `grant_type=password&username=${encodeURIComponent(username)}&password=${encodeURIComponent(password)}`;
    return this.http.post(this.url, body, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
        .set('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD))
    });
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatIconModule,
  MatToolbarModule,
  MatMenuModule,
  MatCardModule,
  MatDividerModule,
  MatTableModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatSelectModule,
  MatSnackBarModule

} from '@angular/material';
import { HomeComponent } from './components/home/home.component';
import { ItemComponent } from './components/shared/item/item.component';
import { LoginComponent } from './components/account/login/login.component';
import { RegisterComponent } from './components/account/register/register.component';
import { ProfileComponent } from './components/account/profile/profile.component';
import { ListProductsComponent } from './components/products/list-products/list-products.component';
import { MyShoppingComponent } from './components/products/my-shopping/my-shopping.component';
import { StockComponent } from './components/products/stock/stock.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HttpClientModule} from '@angular/common/http';
import { AddArticuloComponent } from './components/products/add-articulo/add-articulo.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ItemComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    ListProductsComponent,
    MyShoppingComponent,
    StockComponent,
    AddArticuloComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatMenuModule,
    MatCardModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatDividerModule,
    HttpClientModule,
    MatTableModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSnackBarModule
  ],
  entryComponents: [AddArticuloComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

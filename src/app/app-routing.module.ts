import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {LoginComponent} from './components/account/login/login.component';
import {RegisterComponent} from './components/account/register/register.component';
import {ProfileComponent} from './components/account/profile/profile.component';
import {ListProductsComponent} from './components/products/list-products/list-products.component';
import {MyShoppingComponent} from './components/products/my-shopping/my-shopping.component';
import {StockComponent} from './components/products/stock/stock.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'listar-productos', component: ListProductsComponent },
  { path: 'mis-compras', component: MyShoppingComponent },
  { path: 'stock', component: StockComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', redirectTo: '/home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../../services/login.service';
import {TOKEN_NAME} from '../../../variables/var.constant';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor( private formBuilder: FormBuilder, private loginService: LoginService, private router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.maxLength(50), Validators.email]],
      password: ['',[Validators.required, Validators.minLength(5)]]
    });
  }

  login(): void {
    const username = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;

    this.loginService.login(username, password).subscribe(
      resp => {
        const token = JSON.stringify(resp);
        sessionStorage.setItem(TOKEN_NAME, token);

        this.router.navigate(['/home']);
      });
  }
}

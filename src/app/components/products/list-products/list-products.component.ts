import { Component, OnInit } from '@angular/core';
import {ArticuloService} from '../../../services/articulo.service';
import {Articulo} from '../../../models/articulo';
import {MatDialog, MatSnackBar} from '@angular/material';
import {AddArticuloComponent} from '../add-articulo/add-articulo.component';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.scss']
})
export class ListProductsComponent implements OnInit {
  articulos: Articulo[] = new Array();
  displayedColumns: string[] = ['nombre', 'descripcion', 'precio', 'cantidad', 'acciones'];
  constructor(
    private articuloService: ArticuloService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
    ) { }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddArticuloComponent, {
      width: '650px',
      data: {name: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.openSnackBar('Producto guardado correctamente', 'OK');
        this.articuloService.listar().subscribe(resp => {
          this.articulos = resp;
        });
      }
      // this.animal = result;
    });
  }

  ngOnInit() {
    this.articuloService.listar().subscribe(resp => {
      this.articulos = resp;
    });
  }

  openSnackBar(mensaje: string, action: string) {
    return this.snackBar.open(
      mensaje, action, {
        verticalPosition: 'top',
        duration: 6000
      }
    );
  }
  editar(articulo: Articulo): void{
    const dialogRef = this.dialog.open(AddArticuloComponent, {
      width: '650px',
      data: { articulo: articulo }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.openSnackBar('Producto modificado correctamente', 'OK');
        this.articuloService.listar().subscribe(resp => {
          this.articulos = resp;
        });
      }
      // this.animal = result;
    });
  }
}

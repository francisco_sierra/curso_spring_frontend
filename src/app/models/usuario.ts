export class Categoria {
  id?: number;
  email?: string;
  password?: string;
  telefono?: string;
  nombres?: string;
  apellidos?: string;
  direccion?: string;
  rol?: any;
}
